FROM busybox
ARG site
ADD $site /var/
WORKDIR /var/www
#VOLUME /var/www
EXPOSE 80
CMD httpd -h /var/www;tail -f /dev/null
